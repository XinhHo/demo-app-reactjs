import './add-item.scss';
import { useState } from 'react';
import Item from '../item/item';
function AddItem() {
    const [value, setValue] = useState('');
    const [list, setList] = useState([]);

    const handelChangeInput = (event) => {
        setValue(event.target.value);
    }
    const handelAddData = (event) => {
        const draffList = [...list];
        draffList.push(value);
        setValue('');
        setList(draffList);
    }
    const removeItem = (index) => {
        const array = [...list];
        const listAfterRemove = array.filter((item, i) => i !== index );
        setList(listAfterRemove);
    }
    const example = () => {
        
        if(list && list.length > 0){
            return  (
                <div>
                    {list.map((item, index) => (
                        <Item value={item} key={index} removeItem={item => removeItem(index)}></Item>
                    ))
                    }
                </div>
                   
                )
        } else{
            return (<p>No data to show</p>)
        }
    }

    return (
    <div>
        <div className="add-item">
            <input type="text" className="add-name-label" onChange={ e => handelChangeInput(e) } value={value}/>
            <button className="bnt btn-primary add-name-btn" onClick={e => handelAddData(e)}>Submit</button>
        </div>
        {/* { list && list.length > 0 ? <Item></Item> : <p>No data to show</p>} */}
        <div className="list-item">
            <div className="header boid">List Item</div>
            {example()}
        </div>
    </div>
    );
}
export default AddItem;